FROM python:latest

RUN curl -sL https://github.com/openfaas/faas/releases/download/0.9.14/fwatchdog > /usr/bin/fwatchdog \
    && chmod +x /usr/bin/fwatchdog

ENV fprocess="python entrypoint.py"
COPY entrypoint.py /
COPY requirements.txt /

RUN pip install -r requirements.txt

EXPOSE 8080
CMD [ "fwatchdog" ]