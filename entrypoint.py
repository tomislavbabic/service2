import os
import requests
import sys

SERVICE1_URL = os.environ.get("SERVICE1_URL") or "http://127.0.0.1:8080"

message = requests.get(sys.stdin.readline()).text
data = ["md5", message]

print(SERVICE1_URL, requests.post(SERVICE1_URL, data="\n".join(data).encode('utf-8')).content)